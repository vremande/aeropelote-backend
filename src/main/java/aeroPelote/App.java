package aeroPelote;

import java.io.FileInputStream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@SpringBootApplication
@ComponentScan({ "aeroPelote.controller", "aeroPelote.service" })
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);

		try {

//			FileInputStream serviceAccount = new FileInputStream(
//					"./aeropelote/src/main/resources/aeropelote-firebase-adminsdk-wef0c-a48da6842d.json");
			FileInputStream serviceAccount = new FileInputStream(
					"C:\\Work\\Projets\\BAP\\aeropelote-backend\\src\\main\\resources\\aeropelote-firebase-adminsdk-wef0c-a48da6842d.json");

			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount)).build();

			FirebaseApp.initializeApp(options);
		} catch (Exception e) {
			// TODO gérer l'exception
			System.out.println(e.getMessage());
		}

	}
}