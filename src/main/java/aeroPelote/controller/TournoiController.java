package aeroPelote.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import aeroPelote.model.Classement;
import aeroPelote.model.Confrontation;
import aeroPelote.model.Joueur;
import aeroPelote.model.Tournoi;
import aeroPelote.service.TournoiService;

@RestController
public class TournoiController {
	@Autowired
	TournoiService tournoiService;

	/**
	 * Retourne le classement d'un tournoi
	 * 
	 * @param idTournoi ID du tournoi
	 * @return une liste de classement non triée
	 * @throws Exception
	 */
	@RequestMapping(value = "/tournoi/{id}/classement", method = RequestMethod.GET)
	List<Classement> classement(@PathVariable("id") String idTournoi) throws Exception {
		return tournoiService.getClassement(idTournoi);
	}

	/**
	 * Retourne l'ID du tournoi
	 * 
	 * @param tournoi objet contenant les données d'un tournoi
	 * @return ID du tournoi
	 * @throws Exception
	 */
	@RequestMapping(value = "/tournoi", method = RequestMethod.POST)
	String creerTournoi(@RequestBody Tournoi tournoi) throws Exception {
		return tournoiService.creerTournoi(tournoi);
	}

	/**
	 * Retourne un tournoi
	 * 
	 * @param idTournoi ID du tournoi
	 * @return Le tournoi
	 * @throws Exception
	 */
	@RequestMapping(value = "/tournoi/{id}", method = RequestMethod.GET)
	Tournoi getTournoi(@PathVariable("id") String idTournoi) throws Exception {
		return tournoiService.getTournoi(idTournoi);
	}

	/**
	 * Retoune la liste des tournois d'un administrateur
	 * 
	 * @param idAdmin ID google de l'administrateur
	 * @return la liste des tournois de l'administrateur
	 * @throws Exception
	 */
	@RequestMapping(value = "/tournoi/admin/{id}", method = RequestMethod.GET)
	List<Tournoi> getTournois(@PathVariable("id") String idAdmin) throws Exception {
		return tournoiService.getTournois(idAdmin);
	}

	/**
	 * Supprime un tournoi
	 * 
	 * @param idTournoi ID du tournoi
	 * @throws Exception
	 */
	@RequestMapping(value = "/tournoi/{id}", method = RequestMethod.DELETE)
	void deleteTournoi(@PathVariable("id") String idTournoi) throws Exception {
		tournoiService.supprimerTournoi(idTournoi);
	}

	/**
	 * Ajoute un joueur à un tournoi
	 * 
	 * @param joueur    joueur contenant les informations du joueur à ajouter
	 * @param idTournoi ID tu Tournoi auquel participe le joueur
	 * @throws Exception
	 */
	@RequestMapping(value = "/tournoi/{idTournoi}/joueur", method = RequestMethod.PUT)
	void ajouterJoueur(@RequestBody Joueur joueur, @PathVariable(value = "idTournoi") String idTournoi)
			throws Exception {
		tournoiService.ajouterJoueur(idTournoi, joueur);
	}

	/**
	 * Ajoute des joueurs à un tournoi
	 * 
	 * @param joueur    joueur contenant les informations du joueur à ajouter
	 * @param idTournoi ID tu Tournoi auquel participe le joueur
	 * @throws Exception
	 */
	@RequestMapping(value = "/tournoi/{idTournoi}/joueurs", method = RequestMethod.PUT)
	void ajouterJoueurs(@RequestBody List<Joueur> joueurs, @PathVariable(value = "idTournoi") String idTournoi)
			throws Exception {
		tournoiService.ajouterJoueurs(idTournoi, joueurs);
	}

	/**
	 * Génére les matchs d'un tournoi
	 * 
	 * @param idTournoi ID tu Tournoi auquel participe le joueur
	 * @return génére les matchs d'un tournoi à partir d'une liste de participants
	 * @throws Exception
	 */
	@RequestMapping(value = "/tournoi/{idTournoi}/generer", method = RequestMethod.PUT)
	List<Confrontation> genererTournoi(@PathVariable(value = "idTournoi") String idTournoi) throws Exception {
		return tournoiService.genererTournoi(idTournoi);
	}

}
