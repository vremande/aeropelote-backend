package aeroPelote.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import aeroPelote.model.Confrontation;
import aeroPelote.model.Joueur;
import aeroPelote.service.MatchService;

@RestController
public class MatchController {

	@Autowired
	MatchService matchService;

	/**
	 * Liste de matchs d'un joueur
	 * 
	 * @param joueur    Critère pour récupérer la liste des matchs d'un joueur
	 * @param idTournoi ID du tournoi
	 * @return la liste des matchs d'un joueur
	 * @throws Exception
	 */
	@RequestMapping(value = "/match/{idTournoi}/joueur", method = RequestMethod.GET)
	List<Confrontation> getMatchs(@RequestBody Joueur joueur, @PathVariable("idTournoi") String idTournoi)
			throws Exception {
		return matchService.getMatchs(idTournoi, joueur);
	}

	@RequestMapping(value = "/match/{idTournoi}/joueur/{nomJoueur}", method = RequestMethod.GET)
	List<Confrontation> getMatchs(@PathVariable("idTournoi") String idTournoi,
			@PathVariable("nomJoueur") String nomJoueur) throws Exception {
		return matchService.getMatchs(idTournoi, nomJoueur);
	}

	@RequestMapping(value = "/match/{idTournoi}/joueur1/{nomJoueur1}/joueur2/{nomJoueur2}", method = RequestMethod.GET)
	Confrontation getMatch(@PathVariable("idTournoi") String idTournoi, @PathVariable("nomJoueur1") String nomJoueur1,
			@PathVariable("nomJoueur2") String nomJoueur2) throws Exception {
		return matchService.getMatch(idTournoi, nomJoueur1, nomJoueur2);
	}

	/**
	 * Mettre à jour un match
	 * 
	 * @param match     met à jour un match (date, score, ...) .Doit contenir l'ID
	 *                  du match
	 * @param idTournoi ID du tournoi
	 * @return Le match mis à jour
	 * @throws Exception
	 */
	@RequestMapping(value = "/match/{idTournoi}", method = RequestMethod.PUT)
	Confrontation updateMatch(@RequestBody Confrontation match, @PathVariable("idTournoi") String idTournoi)
			throws Exception {
		return matchService.updateMatch(idTournoi, match);
	}

}
