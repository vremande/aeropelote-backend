package aeroPelote.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;

import aeroPelote.model.Confrontation;
import aeroPelote.model.Joueur;

@Service
public class MatchServiceImpl implements MatchService {

	@Override
	public List<Confrontation> getMatchs(String idTournoi, Joueur joueur) throws Exception {
		List<Confrontation> matchs = new ArrayList<>();

		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(idTournoi);
		CollectionReference tournois = docRef.collection("matchs");

		String nomJoueur = joueur.getPrenom() == null ? "" : joueur.getPrenom();
		nomJoueur = nomJoueur.concat(joueur.getPrenom() == null ? "" : " ");
		nomJoueur = nomJoueur.concat(joueur.getNom() == null ? "" : joueur.getNom());

		Query query1 = tournois.whereEqualTo("joueur1", nomJoueur);
		ApiFuture<QuerySnapshot> querySnapshot1 = query1.get();

		for (DocumentSnapshot document : querySnapshot1.get().getDocuments()) {
			Confrontation match = new Confrontation();
			match.setId(document.getId());
			match.setJoueur1(document.getString("joueur1"));
			match.setScore1(document.getDouble("score1") == null ? null : document.getDouble("score1").intValue());
			match.setJoueur2(document.getString("joueur2"));
			match.setScore2(document.getDouble("score2") == null ? null : document.getDouble("score2").intValue());
			matchs.add(match);
		}

		Query query2 = tournois.whereEqualTo("joueur2", nomJoueur);
		ApiFuture<QuerySnapshot> querySnapshot2 = query2.get();

		for (DocumentSnapshot document : querySnapshot2.get().getDocuments()) {
			Confrontation match = new Confrontation();
			match.setId(document.getId());
			match.setJoueur1(document.getString("joueur1"));
			match.setScore1(document.getDouble("score1") == null ? null : document.getDouble("score1").intValue());
			match.setJoueur2(document.getString("joueur2"));
			match.setScore2(document.getDouble("score2") == null ? null : document.getDouble("score2").intValue());
			matchs.add(match);
		}

		return matchs;
	}

	@Override
	public List<Confrontation> getMatchs(String idTournoi, String nomJoueur) throws Exception {
		List<Confrontation> matchs = new ArrayList<>();

		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(idTournoi);
		CollectionReference tournois = docRef.collection("matchs");

		ApiFuture<QuerySnapshot> queryAll = tournois.get();
		for (DocumentSnapshot document : queryAll.get().getDocuments()) {
			if (document.getString("joueur1").endsWith(nomJoueur)
					|| document.getString("joueur2").endsWith(nomJoueur)) {

				Confrontation match = new Confrontation();
				match.setId(document.getId());
				match.setJoueur1(document.getString("joueur1"));
				match.setScore1(document.getDouble("score1") == null ? null : document.getDouble("score1").intValue());
				match.setJoueur2(document.getString("joueur2"));
				match.setScore2(document.getDouble("score2") == null ? null : document.getDouble("score2").intValue());
				matchs.add(match);
			}
		}

		return matchs;
	}

	@Override
	public Confrontation getMatch(String idTournoi, String nomJoueur1, String nomJoueur2) throws Exception {
		Confrontation match = null;

		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(idTournoi);
		CollectionReference tournois = docRef.collection("matchs");

		ApiFuture<QuerySnapshot> queryAll = tournois.get();
		for (DocumentSnapshot document : queryAll.get().getDocuments()) {
			if ((document.getString("joueur1").endsWith(nomJoueur1)
					&& document.getString("joueur2").endsWith(nomJoueur2))
					|| (document.getString("joueur1").endsWith(nomJoueur2)
							&& document.getString("joueur2").endsWith(nomJoueur1))) {

				match = new Confrontation();
				match.setId(document.getId());
				match.setJoueur1(document.getString("joueur1"));
				match.setScore1(document.getDouble("score1") == null ? null : document.getDouble("score1").intValue());
				match.setJoueur2(document.getString("joueur2"));
				match.setScore2(document.getDouble("score2") == null ? null : document.getDouble("score2").intValue());

				break;
			}
		}

		return match;
	}

	@Override
	public Confrontation updateMatch(String idTournoi, Confrontation match) throws Exception {

		if (match != null && match.getId() != null && !match.getId().isEmpty()) {
			Firestore db = FirestoreClient.getFirestore();

			String idMatch = match.getId();

			DocumentReference tournoi = db.collection("tournois").document(idTournoi);
			DocumentReference matchDocument = tournoi.collection("matchs").document(idMatch);

			DocumentSnapshot document = null;
			if (match.getScore1() != null) {
				ApiFuture<WriteResult> updateScore1 = matchDocument.update("score1", match.getScore1());
				do {
					document = matchDocument.get().get();
				} while (!updateScore1.isDone());
			}
			if (match.getScore2() != null) {
				ApiFuture<WriteResult> updateScore2 = matchDocument.update("score2", match.getScore2());
				do {
					document = matchDocument.get().get();
				} while (!updateScore2.isDone());
			}
			if (match.getDateMatch() != null) {
				ApiFuture<WriteResult> updateDate = matchDocument.update("dateMatch", match.getDateMatch());
				do {
					document = matchDocument.get().get();
				} while (!updateDate.isDone());
			}

			return document.toObject(Confrontation.class);
		} else {
			return null;
		}
	}

}