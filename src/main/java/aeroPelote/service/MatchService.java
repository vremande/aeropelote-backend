package aeroPelote.service;

import java.util.List;

import aeroPelote.model.Confrontation;
import aeroPelote.model.Joueur;

public interface MatchService {

	List<Confrontation> getMatchs(String idTournoi, Joueur joueur) throws Exception;

	List<Confrontation> getMatchs(String idTournoi, String nomJoueur) throws Exception;

	Confrontation getMatch(String idTournoi, String nomJoueur1, String nomJoueur2) throws Exception;

	Confrontation updateMatch(String idTournoi, Confrontation match) throws Exception;

}