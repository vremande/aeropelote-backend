package aeroPelote.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;

import aeroPelote.model.Classement;
import aeroPelote.model.Confrontation;
import aeroPelote.model.Joueur;
import aeroPelote.model.Tournoi;

@Service
public class TournoiServiceImpl implements TournoiService {

	@Autowired
	MatchService matchService;

	@Override
	public Tournoi getTournoi(String id) throws Exception {

		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(id);

		Iterable<DocumentReference> listDocuments = db.collection("tournois").listDocuments();
		for (DocumentReference documentReference : listDocuments) {
			// asynchronously retrieve the document
			ApiFuture<DocumentSnapshot> futureLecture = docRef.get();

			DocumentSnapshot document = futureLecture.get();

			Tournoi tournoiIt = new Tournoi();
			tournoiIt.setId(document.getId());
			tournoiIt.setIdAdmin(document.getString("idAdmin"));
			tournoiIt.setNom(document.getString("nom"));
			tournoiIt.setSpecialite(document.getString("specialite"));
			tournoiIt.setPlace(document.getString("place"));
			tournoiIt.setParticipants((List) document.get("participants"));

			System.out.println(tournoiIt.toString());
		}

		// asynchronously retrieve the document
		ApiFuture<DocumentSnapshot> futureLecture = docRef.get();

		DocumentSnapshot document = futureLecture.get();

		Tournoi tournoi = new Tournoi();
		tournoi.setId(document.getId());
		tournoi.setIdAdmin(document.getString("idAdmin"));
		tournoi.setNom(document.getString("nom"));
		tournoi.setSpecialite(document.getString("specialite"));
		tournoi.setPlace(document.getString("place"));
		tournoi.setParticipants((List) document.get("participants"));

		return tournoi;
	}

	@Override
	public List<Tournoi> getTournois(String idAdmin) throws Exception {

		List<Tournoi> tournoisRetour = new ArrayList<>();

		Firestore db = FirestoreClient.getFirestore();

		CollectionReference tournois = db.collection("tournois");

		CollectionReference admins = db.collection("admins");

		Query query = tournois.whereEqualTo("idAdmin", idAdmin);
		ApiFuture<QuerySnapshot> querySnapshot = query.get();

		for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
			Tournoi tournoi = new Tournoi();
			tournoi.setId(document.getId());
			tournoi.setIdAdmin(document.getString("idAdmin"));
			tournoi.setNom(document.getString("nom"));
			tournoi.setSpecialite(document.getString("specialite"));
			tournoi.setPlace(document.getString("place"));
			tournoi.setParticipants((List) document.get("participants"));
			tournoisRetour.add(tournoi);
		}

		return tournoisRetour;
	}

	@Override
	public List<Classement> getClassement(String idTournoi) throws Exception {
		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(idTournoi);

		ApiFuture<QuerySnapshot> future = docRef.collection("matchs").get();

		Map<String, Classement> classement = new HashMap<>();

		for (DocumentSnapshot document : future.get().getDocuments()) {
			Confrontation match = document.toObject(Confrontation.class);
			if (match.getScore1() == null || match.getScore2() == null) {
				classement.putIfAbsent(match.getJoueur1(), null);
				classement.putIfAbsent(match.getJoueur2(), null);
			} else if (match.getScore1() != match.getScore2()) {
				String vainqueur = match.getJoueur1();
				String perdant = match.getJoueur2();
				if (match.getScore2() > match.getScore1()) {
					vainqueur = match.getJoueur2();
					perdant = match.getJoueur1();
				}

				classement.putIfAbsent(vainqueur, new Classement(vainqueur));
				classement.putIfAbsent(perdant, new Classement(perdant));
				classement.compute(vainqueur, (joueur, classementEnCours) -> classementEnCours.matchGagne());
				classement.compute(perdant, (joueur, classementEnCours) -> classementEnCours.matchPerdu());
			} else if (match.getScore1() > 0) {
				classement.putIfAbsent(match.getJoueur1(), new Classement(match.getJoueur1()));
				classement.putIfAbsent(match.getJoueur2(), new Classement(match.getJoueur2()));
				classement.computeIfPresent(match.getJoueur1(),
						(joueur, classementEnCours) -> classementEnCours.matchNul());
				classement.computeIfPresent(match.getJoueur2(),
						(joueur, classementEnCours) -> classementEnCours.matchNul());
			}

		}

		return classement.values().stream().collect(Collectors.toList());
	};

	@Override
	public String creerTournoi(Tournoi tournoi) throws Exception {

		if (tournoi.getIdAdmin() == null) {
			return null;
		}

		Firestore db = FirestoreClient.getFirestore();

		Map<String, Object> docData = new HashMap<>();
		docData.put("idAdmin", tournoi.getIdAdmin());

		docData.put("nom", tournoi.getNom() == null ? "Tournoi" : tournoi.getNom());

		ApiFuture<DocumentReference> addedDocRef = db.collection("tournois").add(docData);

		return addedDocRef.get().getId();
	}

	@Override
	public void ajouterJoueur(String idTournoi, Joueur joueur) throws Exception {

		if (joueur != null) {
			Firestore db = FirestoreClient.getFirestore();

			DocumentReference docRef = db.collection("tournois").document(idTournoi);

			Map<String, Object> docData = new HashMap<>();
			if (joueur.getNom() != null && !joueur.getNom().isEmpty()) {
				docData.put("nom", joueur.getNom());
			}
			if (joueur.getPrenom() != null && !joueur.getPrenom().isEmpty()) {
				docData.put("prenom", joueur.getPrenom());
			}

			docRef.update("participants", FieldValue.arrayUnion(docData));
		}
	}

	@Override
	public void ajouterJoueurCollection(String idTournoi, Joueur joueur) throws Exception {

		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(idTournoi);

		CollectionReference participants = docRef.collection("joueurs");

		Query query = participants.whereEqualTo("nom", joueur.getNom());
		ApiFuture<QuerySnapshot> querySnapshot = query.get();

		if (querySnapshot.get().getDocuments().isEmpty()) {
			Map<String, Object> docData = new HashMap<>();
			docData.put("nom", joueur.getNom());
			docData.put("prenom", joueur.getPrenom());
			ApiFuture<DocumentReference> addedDocRef = docRef.collection("joueurs").add(docData);
		}
	}

	@Override
	public void ajouterJoueurs(String idTournoi, List<Joueur> joueurs) throws Exception {
		for (Joueur joueur : joueurs) {
			ajouterJoueur(idTournoi, joueur);
		}

	}

	@Override
	public List<Confrontation> genererTournoi(String idTournoi) throws Exception {
		List<Confrontation> matchs = new ArrayList<>();

		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(idTournoi);

		ApiFuture<DocumentSnapshot> futureLecture = docRef.get();
		DocumentSnapshot document = futureLecture.get();
		List<Map<String, String>> objets = (List) document.get("participants");

		List<String> participants = new ArrayList<>();
		List<String> opposants = new ArrayList<>();

		for (Map<String, String> objet : objets) {
			String joueur = objet.get("prenom") == null ? "" : objet.get("prenom");
			joueur = joueur.concat(objet.get("prenom") == null ? "" : " ");
			joueur = joueur.concat(objet.get("nom") == null ? "" : objet.get("nom"));
			participants.add(joueur);
		}
		opposants.addAll(participants);

		for (String participant : participants) {
			opposants.remove(participant);
			for (String opposant : opposants) {
				Confrontation match = new Confrontation("1", null);
				Map<String, Object> docData = new HashMap<>();
				docData.put("joueur1", participant);
				docData.put("joueur2", opposant);
				ApiFuture<DocumentReference> addedDocRef = docRef.collection("matchs").add(docData);
				matchs.add(match);

			}
		}

		return matchs;
	}

	@Override
	public List<Confrontation> genererTournoiCollection(String idTournoi) throws Exception {
		List<Confrontation> matchs = new ArrayList<>();

		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(idTournoi);

		// asynchronously retrieve all documents
		ApiFuture<QuerySnapshot> future = db.collection("participants").get();
		List<QueryDocumentSnapshot> documents = future.get().getDocuments();

		List<String> participants = new ArrayList<>();
		List<String> opposants = new ArrayList<>();
		for (QueryDocumentSnapshot document : documents) {
			String joueur = document.get("prenom") == null ? "" : document.getString("prenom");
			joueur = joueur.concat(document.get("prenom") == null ? "" : " ");
			joueur = joueur.concat(document.get("nom") == null ? "" : document.getString("nom"));
			participants.add(joueur);
		}
		opposants.addAll(participants);

		for (String participant : participants) {
			opposants.remove(participant);
			for (String opposant : opposants) {
				Confrontation match = new Confrontation("1", null);
				Map<String, Object> docData = new HashMap<>();
				docData.put("joueur1", participant);
				docData.put("joueur2", opposant);
				ApiFuture<DocumentReference> addedDocRef = docRef.collection("matchs").add(docData);
				matchs.add(match);
			}
		}

		return matchs;
	}

	@Override
	public void supprimerTournoi(String idTournoi) throws Exception {
		// TODO Auto-generated method stub
		Firestore db = FirestoreClient.getFirestore();

		DocumentReference docRef = db.collection("tournois").document(idTournoi);

		docRef.delete();
	}

	@Override
	public void modifierJoueur(String idTournoi, Joueur joueur) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void supprimerJoueur(String idTournoi, Joueur joueur) throws Exception {
		// TODO Auto-generated method stub

	}

}