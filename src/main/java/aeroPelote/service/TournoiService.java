package aeroPelote.service;

import java.util.List;

import aeroPelote.model.Classement;
import aeroPelote.model.Confrontation;
import aeroPelote.model.Joueur;
import aeroPelote.model.Tournoi;

public interface TournoiService {

	Tournoi getTournoi(String id) throws Exception;

	List<Tournoi> getTournois(String idAdmin) throws Exception;

	List<Classement> getClassement(String idTournoi) throws Exception;

	String creerTournoi(Tournoi tournoi) throws Exception;

	void supprimerTournoi(String idTournoi) throws Exception;

	void ajouterJoueur(String idTournoi, Joueur joueur) throws Exception;

	void ajouterJoueurCollection(String idTournoi, Joueur joueur) throws Exception;

	void ajouterJoueurs(String idTournoi, List<Joueur> joueurs) throws Exception;

	void modifierJoueur(String idTournoi, Joueur joueur) throws Exception;

	void supprimerJoueur(String idTournoi, Joueur joueur) throws Exception;

	List<Confrontation> genererTournoi(String idTournoi) throws Exception;

	List<Confrontation> genererTournoiCollection(String idTournoi) throws Exception;
}