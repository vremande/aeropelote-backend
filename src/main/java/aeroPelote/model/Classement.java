package aeroPelote.model;

public class Classement {
    private String nom;
    private int joue;
    private int gagne;
    private int perdu;
    private int nul;
    private int point;

    public Classement() {
    }

    public Classement(String nom) {
        this.nom = nom;
        this.joue = 0;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getJoue() {
        return joue;
    }

    public void setJoue(int joue) {
        this.joue = joue;
    }

    public int getGagne() {
        return gagne;
    }

    public void setGagne(int gagne) {
        this.gagne = gagne;
    }

    public int getPerdu() {
        return perdu;
    }

    public void setPerdu(int perdu) {
        this.perdu = perdu;
    }

    public int getNul() {
        return nul;
    }

    public void setNul(int nul) {
        this.nul = nul;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + gagne;
        result = prime * result + joue;
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + nul;
        result = prime * result + perdu;
        result = prime * result + point;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Classement other = (Classement) obj;
        if (gagne != other.gagne)
            return false;
        if (joue != other.joue)
            return false;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (nul != other.nul)
            return false;
        if (perdu != other.perdu)
            return false;
        if (point != other.point)
            return false;
        return true;
    }

    public Classement matchGagne () {
        this.gagne++;
        this.joue++;
        this.point += 3;

        return this;
    }

    public Classement matchPerdu () {
        this.perdu++;
        this.joue++;

        return this;
    }

    public Classement matchNul () {
        this.nul++;
        this.joue++;
        this.point += 1;

        return this;
    }
}
