package aeroPelote.model;

import java.util.Date;

public class Confrontation {
    private String id;
    private Date dateMatch;
    private String joueur1;
    private Integer score1;
    private String joueur2;
    private Integer score2;

    public Confrontation() {
    }

    public Confrontation(String id, Date dateMatch) {
        this.id = id;
        this.dateMatch = dateMatch;
    }

   public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateMatch() {
        return dateMatch;
    }

    public void setDateMatch(Date dateMatch) {
        this.dateMatch = dateMatch;
    }

    public String getJoueur1() {
        return joueur1;
    }

    public void setJoueur1(String joueur1) {
        this.joueur1 = joueur1;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public String getJoueur2() {
        return joueur2;
    }

    public void setJoueur2(String joueur2) {
        this.joueur2 = joueur2;
    }

    public Integer getScore2() {
        return score2;
    }

    public void setScore2(Integer score2) {
        this.score2 = score2;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateMatch == null) ? 0 : dateMatch.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((joueur1 == null) ? 0 : joueur1.hashCode());
        result = prime * result + ((joueur2 == null) ? 0 : joueur2.hashCode());
        result = prime * result + ((score1 == null) ? 0 : score1.hashCode());
        result = prime * result + ((score2 == null) ? 0 : score2.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Confrontation other = (Confrontation) obj;
        if (dateMatch == null) {
            if (other.dateMatch != null)
                return false;
        } else if (!dateMatch.equals(other.dateMatch))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (joueur1 == null) {
            if (other.joueur1 != null)
                return false;
        } else if (!joueur1.equals(other.joueur1))
            return false;
        if (joueur2 == null) {
            if (other.joueur2 != null)
                return false;
        } else if (!joueur2.equals(other.joueur2))
            return false;
        if (score1 == null) {
            if (other.score1 != null)
                return false;
        } else if (!score1.equals(other.score1))
            return false;
        if (score2 == null) {
            if (other.score2 != null)
                return false;
        } else if (!score2.equals(other.score2))
            return false;
        return true;
    }



}
